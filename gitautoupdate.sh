# this script gets called on a schedule by Node-Red to ensure changes are kept up to date in the repo.  
# Use responsibly!
cd /config

git add .
git status
git commit -a -m "Auto-Backup"
git push origin master


exit